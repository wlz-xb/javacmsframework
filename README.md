#基于actframework的权限管理系统
#权限采用的是java actframework框架，数据库采用的是mysql(其操作使用的是ebean)
#数据库是在zanha.sql，自行创建数据库，编码是utf8
#数据库配置文件地址在/src/main/resources/conf/common/db.properties文件
<p>
<code>
#db.url=jdbc:h2:./test<br/>
#db.ddl.createOnly=true<br/>
# If you have only one DBPlugin in your class path, then<br/>
# you do not need to specify the db.impl configuration<br/>
#db.impl=act.db.ebean.EbeanPlugin<br/>
# database driver default to org.h2.Driver<br/>
db.driver=com.mysql.jdbc.Driver<br/>
# database Url default to jdbc:h2:mem:tests<br/>
db.url=jdbc:mysql://127.0.0.1:3306/zanha?useUnicode=true&characterEncoding=UTF-8&useSSL=false<br/>
# username default is empty<br/>
db.username=root<br/>
# password default is empty<br/>
db.password=root<br/>
# If specified then app scan package will be used instead<br/>
db.db2.agentPackage=com.zanha.javacmsframework.model.**<br/>
</code>
</p>
#后台访问地址:http://localhost:8080/admin/Login/index<br/>
#后台的账号是testtest,密码是testteset
<br/>
**2017年3月21日修复如下问题**<br/>
1.修复了后台登陆地址为 http://localhost:8080/admin/Login<br/>
2.修复了http://localhost:8080/ 为默认的后台地址<br/>
3.修复后台登陆密码是明文的问题，现在改成了加密的，并且密码的时候还添加了safe_key，非常安全了<br/>
4.修改genSafeKey为9个随机字符，这样更加安全，这样密码更加安全了