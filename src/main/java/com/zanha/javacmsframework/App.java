package com.zanha.javacmsframework;
import act.app.conf.AppConfigurator;
import act.boot.app.RunApp;
import act.security.CSRFProtector;
import static com.zanha.javacmsframework.App.GLOBAL_CORS.*;

/**
 * app入口文件
 * @author 粽子
 */
public class App extends AppConfigurator<App>{
	public static class GLOBAL_CORS {
        public static final String ALLOW_ORIGIN = "*";
        public static final String ALLOW_EXPOSE_HEADER = "X-Header-One";
        public static final String MAX_AGE = "100";
    }

	/**
	 * 基本配置啦
     * @author 粽子
	 */
    @Override
    public void configure() {
        csrf().disable().protector(CSRFProtector.Predefined.RANDOM);
        cors().allowOrigin(ALLOW_ORIGIN).allowAndExposeHeaders(ALLOW_EXPOSE_HEADER).maxAge(Integer.parseInt(MAX_AGE));
    }

    /**
     * 入口啦
     * @param args
     * @throws Exception
     * @author 粽子
     */
    public static void main(String[] args) throws Exception {
      	RunApp.start("zanha","1.0.4",App.class);
    }
}