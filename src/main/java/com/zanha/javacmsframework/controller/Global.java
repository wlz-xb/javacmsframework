package com.zanha.javacmsframework.controller;
import static act.controller.Controller.Util.renderTemplate;
import javax.inject.Inject;
import act.app.ActionContext;
import act.view.RenderAny;

public class Global {
	@Inject
	ActionContext context;
	
	/**
	 * 判断是否有权限啊，如果有权限才可以访问啊
	 * @author 粽子
	 * @return 
	 */
	public RenderAny errorMsg(String msg,String url,Integer t){		
		return this.tpl("errormsg.html",msg,url,t);
	}
	
	/**
	 * 判断是否有权限啊，如果有权限才可以访问啊
	 * @author 粽子
	 * @return 
	 */
	public RenderAny successMsg(String msg,String url,Integer t){		
		return this.tpl("successmsg.html",msg,url,t);
	}	

	/**
	 * 调用模板啦
	 * @param args
	 * @return
	 */
	public RenderAny tpl(Object... args){
		return renderTemplate(args);
	}
}