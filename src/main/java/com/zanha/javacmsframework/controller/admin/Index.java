package com.zanha.javacmsframework.controller.admin;
import org.osgl.mvc.annotation.GetAction;
import act.view.RenderAny;

/**
 * 后台主框架
 * @author 粽子
 */
public class Index extends Base{
	/**
	 * 后台首页
	 * @return
	 * @author 粽子
	 */
	@GetAction(value={"/","Index/index","Index"})
	public RenderAny index(){				
		return this.tpl("/admin/index.html");
	}	
	
	/**
	 * 后台欢迎页面
	 * @return
	 * @author 粽子
	 */
	@GetAction("Index/welcome")
	public RenderAny welcome(){
		return this.tpl("/admin/welcome.html");
	}		
}