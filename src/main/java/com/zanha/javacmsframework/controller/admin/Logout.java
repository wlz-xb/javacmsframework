package com.zanha.javacmsframework.controller.admin;
import javax.inject.Inject;
import org.osgl.http.H;
import org.osgl.mvc.annotation.GetAction;
import act.controller.Controller.Util;

/**
 * 登录退出
 * @author 粽子
 */
public class Logout{
	@Inject
    private H.Session session;
	
	/**
	 * 登录退出
	 * @return
	 * @author 粽子
	 */
	@GetAction("/admin/Logout/index")
	public void index(){			
		session.clear();
        throw Util.redirect("/admin/Index/index");
	}	
}
