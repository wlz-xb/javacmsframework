package com.zanha.javacmsframework.controller.admin;
import static act.controller.Controller.Util.renderTemplate;
import org.osgl.mvc.annotation.GetAction;
import act.view.RenderAny;

public class Ua {
	/**
	 * 浏览器拒绝页面
	 * @return
	 * @author 粽子
	 */
	@GetAction("/admin/Ua/index")
	public RenderAny index(){			
		return renderTemplate("/admin/ua.html");
	}
}