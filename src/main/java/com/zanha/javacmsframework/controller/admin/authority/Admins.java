package com.zanha.javacmsframework.controller.admin.authority;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

import com.zanha.javacmsframework.logic.AdminsLogic;
import org.osgl.mvc.annotation.GetAction;
import org.osgl.mvc.annotation.PostAction;
import com.alibaba.fastjson.JSONObject;
import com.zanha.javacmsframework.controller.admin.Base;
import com.zanha.javacmsframework.model.authority.AdminsModel;
import com.zanha.javacmsframework.model.authority.AdminsRulesModel;
import com.zanha.javacmsframework.util.BBeanUtils;
import com.zanha.javacmsframework.util.Power;
import act.app.ActionContext;
import act.controller.Controller.Util;
import act.db.ebean.EbeanDao;
import act.view.RenderAny;

/**
 * 权限用户
 * @author 粽子
 */
public class Admins extends Base{
	@Inject
    private EbeanDao<Integer,AdminsModel> adminsDao;
	
	@Inject
    private EbeanDao<Integer,AdminsRulesModel> adminsRulesDao;	
	
	/**
	 * 列表【获取列表的数据啦】
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/Admins/index")
	public RenderAny index(){			
		List<AdminsModel> admins = adminsDao.findAllAsList();	
		return this.tpl("/admin/authority/Admins/index.html",admins);	
	}	
	
	/**
	 * 添加	 
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/Admins/add")
	public RenderAny add(){		
		AdminsModel admin = new AdminsModel();
		
		//添加之前显示出当前的角色啦
		List<AdminsRulesModel> adminsRules = adminsRulesDao.findAllAsList();
		
		return this.tpl("/admin/authority/Admins/post.html",admin,adminsRules);	
	}
	
	/**
	 * 修改	 
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/Admins/edit")
	public RenderAny edit(Integer adminid){
		AdminsModel admin = adminsDao.findById(adminid);
		
		//修改之前显示出当前的角色啦
		List<AdminsRulesModel> adminsRules = adminsRulesDao.findAllAsList();
		
		return this.tpl("/admin/authority/Admins/post.html",admin,adminsRules);	
	}
	
	/**
	 * 删除	 
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/Admins/delete")
	public void delete(Integer adminid){
		adminsDao.deleteById(adminid);	
		Util.redirect("authority.Admins.index");
	}
	
	/**
	 * 添加的逻辑啦
	 * @param admindata
	 * @return
	 * @author 粽子
	 * @throws Exception 
	 */
	@PostAction("authority/Admins/addHandler")
	public JSONObject addHandler(AdminsModel admindata,ActionContext context){
		return this.handler(admindata,context);
	}	
	
	/**
	 * 修改的逻辑啦
	 * @param admindata
	 * @return
	 * @author 粽子
	 * @throws Exception 
	 */
	@PostAction("authority/Admins/editHandler")
	public JSONObject editHandler(@Valid AdminsModel admindata,ActionContext context){
		return this.handler(admindata,context);
	}
	
	/**
	 * 正在的添加修改处理逻辑啊
	 * return JSONObject
	 * @author 粽子
	 * @throws Exception 
	 */
	private final JSONObject handler(@Valid AdminsModel admindata,ActionContext context){	
		Integer adminid = admindata.getAdminid();
		JSONObject result = new JSONObject();		
		if(context.hasViolation()){			
			result.put("errcode", 1);
	        result.put("msg",context.violationMessage());	
	        return result;
		}

		//生成一个safekey
		AdminsLogic adminsLogic = new AdminsLogic();
		String safekey = adminsLogic.genSafeKey();
		
		AdminsModel admin,oneadmin;
		if(adminid != null){
			//当前的权限用户
			oneadmin = adminsDao.findById(adminid);

			//在修改之前先去判断，如果密码传过来的值是空的，那么密码就保持原来的
			if(admindata.getPassword() == null || admindata.getPassword().trim().isEmpty()){
				admindata.setPassword(oneadmin.getPassword());
			}else{
				String newpassword = adminsLogic.encodePassword(admindata.getPassword(),safekey);
				admindata.setPassword(newpassword);
				admindata.setSafekey(safekey);
			}
			
			Util.notFoundIfNull(oneadmin);
			try {
				BBeanUtils.copyPropertyss(admindata,oneadmin);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//修改权限用户
			oneadmin.setUpdatetime(Power.getThisTime());
			admin = adminsDao.save(oneadmin);
		}else{
			String newpassword = adminsLogic.encodePassword(admindata.getPassword(),safekey);
			admindata.setPassword(newpassword);
			admindata.setSafekey(safekey);
			admindata.setAddtime(Power.getThisTime());	
			admindata.setUpdatetime(Power.getThisTime());
			admin = adminsDao.save(admindata);
		}			
		if(admin.getAdminid() > 0){					
			result.put("errcode", 0);
	        result.put("msg", "保存成功，正在为您跳转到权限用户列表！");
		}else{
			result.put("errcode", 1);
	        result.put("msg", "保存失败");			
		}
		return result;
	}
}