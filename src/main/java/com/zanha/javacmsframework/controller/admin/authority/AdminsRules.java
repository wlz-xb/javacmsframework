package com.zanha.javacmsframework.controller.admin.authority;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.osgl.mvc.annotation.GetAction;
import org.osgl.mvc.annotation.PostAction;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.annotation.Transactional;
import com.zanha.javacmsframework.controller.admin.Base;
import com.zanha.javacmsframework.logic.AdminsRulesSourcesLogic;
import com.zanha.javacmsframework.model.authority.AdminsRulesDetailsModel;
import com.zanha.javacmsframework.model.authority.AdminsRulesModel;
import com.zanha.javacmsframework.model.authority.AdminsRulesSourcesModel;
import com.zanha.javacmsframework.util.BBeanUtils;
import com.zanha.javacmsframework.util.Power;
import act.controller.Controller.Util;
import act.db.ebean.EbeanDao;
import act.view.RenderAny;

/**
 * 权限角色
 * @author 粽子
 */
public class AdminsRules extends Base{
	@Inject
    private EbeanDao<Integer,AdminsRulesModel> adminsRulesDao;
		
	@Inject
    private EbeanDao<Integer,AdminsRulesSourcesModel> adminsRulesSourcesDao;
	
	@Inject
	private EbeanDao<Integer,AdminsRulesDetailsModel> adminsRulesDetailsDao;	
	
	//当前的权限资源列表啦
	private List<AdminsRulesSourcesModel> adminsRulesSources = null;
	
	//逻辑部分的导入啦
	private AdminsRulesSourcesLogic adminsRulesSourcesLogic = new AdminsRulesSourcesLogic();
	
	/**
	 * 列表【获取列表的数据啦】
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRules/index")
	public RenderAny index(){	
		List<AdminsRulesModel> adminsRules = adminsRulesDao.findAllAsList();	
		return this.tpl("/admin/authority/AdminsRules/index.html",adminsRules);	
	}
	
	/**
	 * 添加	 
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRules/add")
	public RenderAny add(){
		AdminsRulesModel adminsRule = new AdminsRulesModel();
		adminsRulesSources = adminsRulesSourcesLogic.getAllAdminsRulesSources(adminsRulesSourcesDao.findAllAsList(),0,0);
		return this.tpl("/admin/authority/AdminsRules/post.html",adminsRule,adminsRulesSources);
	}
	
	/**
	 * 修改	 
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRules/edit")
	public RenderAny edit(Integer ruleid){
		AdminsRulesModel adminsRule = adminsRulesDao.findById(ruleid);
		adminsRulesSources = adminsRulesSourcesLogic.getAllAdminsRulesSources(adminsRulesSourcesDao.findAllAsList(),0,0);
		return this.tpl("/admin/authority/AdminsRules/post.html",adminsRule,adminsRulesSources);
	}
	
	/**
	 * 删除一条信息
	 * @param ruleid
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRules/delete")
	public void delete(Integer ruleid){
		adminsRulesDao.deleteById(ruleid);	
		Util.redirect("authority.AdminsRules.index");
	}
	
	/**
	 * 添加的逻辑啦
	 * @param admindata
	 * @return
	 * @author 粽子
	 */
	@PostAction("authority/AdminsRules/addHandler")
	@Transactional
	public JSONObject addHandler(AdminsRulesModel adminsRulesData){
		return this.handler(adminsRulesData);
	}	
	
	/**
	 * 修改的逻辑啦
	 * @param admindata
	 * @return
	 * @author 粽子
	 */
	@PostAction("authority/AdminsRules/editHandler")
	@Transactional
	public JSONObject editHandler(AdminsRulesModel adminsRulesData){
		return this.handler(adminsRulesData);
	}
	
	/**
	 * 正在的添加修改处理逻辑啊
	 * return JSONObject
	 * @author 粽子
	 */
	private final JSONObject handler(AdminsRulesModel adminsRulesData){
		Integer ruleid = adminsRulesData.getRuleid();
		JSONObject result = new JSONObject();
		AdminsRulesModel adminsRule,oneadminsRule;
		if(ruleid != null){
			oneadminsRule = adminsRulesDao.findById(ruleid);
			Util.notFoundIfNull(oneadminsRule);
			try {
				BBeanUtils.copyPropertyss(adminsRulesData,oneadminsRule);
			} catch (Exception e) {
				e.printStackTrace();
			}
			oneadminsRule.setUpdatetime(Power.getThisTime());
			adminsRule = adminsRulesDao.save(oneadminsRule);					
		}else{
			adminsRulesData.setAddtime(Power.getThisTime());
			adminsRulesData.setUpdatetime(Power.getThisTime());
			adminsRule = adminsRulesDao.save(adminsRulesData);
		}			
		
		//成功保存之后吧，将资源的id存放到Details中		
		adminsRulesDetailsDao.deleteBy("ruleid",adminsRule.getRuleid());
		String[] sourceids = adminsRulesData.getSourceids().split(",");
		List<AdminsRulesDetailsModel> detailsdata = new ArrayList<AdminsRulesDetailsModel>();
		for(int i=0;i<sourceids.length;i++){
			AdminsRulesDetailsModel detail = new AdminsRulesDetailsModel();
			detail.setRuleid(ruleid);
			detail.setSourceid(Integer.parseInt(sourceids[i]));			
			detailsdata.add(detail);
		}			
		adminsRulesDetailsDao.save(detailsdata);
		if(adminsRule.getRuleid() > 0){					
			result.put("errcode", 0);
	        result.put("msg", "保存成功，正在为您跳转到权限角色列表！");
		}else{
			result.put("errcode", 1);
	        result.put("msg", "保存失败");			
		}
		return result;
	}
}