package com.zanha.javacmsframework.controller.admin.authority;
import java.util.List;
import javax.inject.Inject;
import org.osgl.mvc.annotation.GetAction;
import org.osgl.mvc.annotation.PostAction;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Ebean;
import com.zanha.javacmsframework.controller.admin.Base;
import com.zanha.javacmsframework.logic.AdminsRulesSourcesLogic;
import com.zanha.javacmsframework.model.authority.AdminsRulesSourcesModel;
import com.zanha.javacmsframework.util.BBeanUtils;
import com.zanha.javacmsframework.util.Power;
import act.controller.Controller.Util;
import act.db.ebean.EbeanDao;
import act.view.RenderAny;

/**
 * 权限资源
 * @author 粽子
 */
public class AdminsRulesSources extends Base{
	@Inject
    private EbeanDao<Integer,AdminsRulesSourcesModel> adminsRulesSourcesDao;	
	
	//当前的权限资源列表啦
	private List<AdminsRulesSourcesModel> adminsRulesSources = null;
	
	//逻辑部分的导入啦
	private AdminsRulesSourcesLogic adminsRulesSourcesLogic = new AdminsRulesSourcesLogic();
	
	/**
	 * 列表【获取列表的数据啦】
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRulesSources/index")
	public RenderAny index(){
		adminsRulesSources = adminsRulesSourcesLogic.getAllAdminsRulesSources(adminsRulesSourcesDao.findAllAsList(),0,0);
		return this.tpl("/admin/authority/AdminsRulesSources/index.html",adminsRulesSources);
	}
	
	/**
	 * 添加	 
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRulesSources/add")
	public RenderAny add(){
		AdminsRulesSourcesModel adminsRulesSource = new AdminsRulesSourcesModel();		
		adminsRulesSources = adminsRulesSourcesLogic.getAllAdminsRulesSources(adminsRulesSourcesDao.findAllAsList(),0,0);		
		return this.tpl("/admin/authority/AdminsRulesSources/post.html",adminsRulesSource,adminsRulesSources);
	}
	
	/**
	 * 修改	 
	 * @return
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRulesSources/edit")
	public RenderAny edit(Integer sourceid){
		AdminsRulesSourcesModel adminsRulesSource = adminsRulesSourcesDao.findById(sourceid);
		List<AdminsRulesSourcesModel> results = Ebean.find(AdminsRulesSourcesModel.class).where().not().in("sourceid",sourceid).findList();
		adminsRulesSources = adminsRulesSourcesLogic.getAllAdminsRulesSources(results,0,0);
		return this.tpl("/admin/authority/AdminsRulesSources/post.html",adminsRulesSource,adminsRulesSources);
	}
	
	/**
	 * 删除一条信息
	 * @param ruleid
	 * @author 粽子
	 */
	@GetAction("authority/AdminsRulesSources/delete")
	public void delete(Integer sourceid){
		adminsRulesSourcesDao.deleteById(sourceid);	
		Util.redirect("authority.AdminsRulesSources.index");
	}
	
	/**
	 * 添加的逻辑啦
	 * @param admindata
	 * @return
	 * @author 粽子
	 */
	@PostAction("authority/AdminsRulesSources/addHandler")
	public JSONObject addHandler(AdminsRulesSourcesModel adminsRulesSourcesData){
		return this.handler(adminsRulesSourcesData);
	}	
	
	/**
	 * 修改的逻辑啦
	 * @param admindata
	 * @return
	 */
	@PostAction("authority/AdminsRulesSources/editHandler")
	public JSONObject editHandler(AdminsRulesSourcesModel adminsRulesSourcesData){
		return this.handler(adminsRulesSourcesData);
	}
	
	/**
	 * 正在的添加修改处理逻辑啊
	 * return JSONObject
	 * @author 粽子
	 */
	private final JSONObject handler(AdminsRulesSourcesModel adminsRulesSourcesData){
		Integer sourceid = adminsRulesSourcesData.getSourceid();
		JSONObject result = new JSONObject();
		AdminsRulesSourcesModel adminsRulesSource,oneadminsRulesSource;
		if(sourceid != null){
			oneadminsRulesSource = adminsRulesSourcesDao.findById(sourceid);
			Util.notFoundIfNull(oneadminsRulesSource);
			try {
				BBeanUtils.copyPropertyss(adminsRulesSourcesData,oneadminsRulesSource);
			} catch (Exception e) {
				e.printStackTrace();
			}
			oneadminsRulesSource.setUpdatetime(Power.getThisTime());
			adminsRulesSource = adminsRulesSourcesDao.save(oneadminsRulesSource);			
		}else{
			adminsRulesSourcesData.setAddtime(Power.getThisTime());
			adminsRulesSourcesData.setUpdatetime(Power.getThisTime());
			adminsRulesSource = adminsRulesSourcesDao.save(adminsRulesSourcesData);
		}			
		if(adminsRulesSource.getSourceid() > 0){					
			result.put("errcode", 0);
	        result.put("msg", "保存成功，正在为您跳转到权限资源列表！");
		}else{
			result.put("errcode", 1);
	        result.put("msg", "保存失败");			
		}
		return result;
	}
}