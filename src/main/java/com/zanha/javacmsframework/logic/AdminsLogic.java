package com.zanha.javacmsframework.logic;
import com.zanha.javacmsframework.util.Power;
import sun.misc.BASE64Encoder;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@SuppressWarnings("restriction")
public class AdminsLogic {
    /**
     * 生成一个safekey啦
     * @return
     */
    public String genSafeKey(){
        return Power.getRandomString(9);
    }

    /**
     * 生成一个md5加密的字符串啦
     * @param password
     * @param safekey
     * @return
     */
	public String encodePassword(String password,String safekey){
        //2个字符串之间链接在一起
        String str = password+safekey;

        //确定计算方法
        String newstr = "";
        try {
            MessageDigest md5= MessageDigest.getInstance("MD5");
            BASE64Encoder base64en = new BASE64Encoder();

            try {
                //加密后的字符串
                newstr=base64en.encode(md5.digest(str.getBytes("utf-8")));
            }catch (UnsupportedEncodingException e){
                return e.getMessage();
            }

        }catch (NoSuchAlgorithmException e){
            return e.getMessage();
        }
        return newstr;
    }

    /**
     * 字符串比较啊
     * @author 粽子
     */
    public boolean checkPassword(String formpass,String thispass){
        return formpass.equals(thispass);
    }
}
