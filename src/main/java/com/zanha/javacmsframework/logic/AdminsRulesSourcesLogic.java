package com.zanha.javacmsframework.logic;
import java.util.ArrayList;
import java.util.List;
import com.zanha.javacmsframework.model.authority.AdminsRulesSourcesModel;

/**
 * 公共的逻辑啊
 * @author 粽子
 */
public class AdminsRulesSourcesLogic{
	//无限极分类中的数据啊
	private List<AdminsRulesSourcesModel> treeList = new ArrayList<AdminsRulesSourcesModel>();
	
	//临时存起来
	private List<AdminsRulesSourcesModel> tmpTreeList = new ArrayList<AdminsRulesSourcesModel>();
	
	/**
	 * 无限极逻辑
	 * @return
	 */
	public List<AdminsRulesSourcesModel> getAllAdminsRulesSources(List<AdminsRulesSourcesModel> data, Integer psourceid,Integer level) {
        for(int i = 0; i<data.size();i++){
            if(data.get(i).getPsourceid()==psourceid){
            	data.get(i).setLevel(level);
                this.treeList.add(data.get(i));
                this.tmpTreeList = data;
                this.getAllAdminsRulesSources(this.tmpTreeList,data.get(i).getSourceid(),level+1);
            } 
        }
        return this.treeList;
    }
}
