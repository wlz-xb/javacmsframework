package com.zanha.javacmsframework.logic;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import org.osgl.http.H;
import com.avaje.ebean.Ebean;
import act.app.ActionContext;
import act.db.ebean.EbeanDao;
import com.zanha.javacmsframework.logic.AdminsRulesSourcesLogic;
import com.zanha.javacmsframework.model.authority.AdminsModel;
import com.zanha.javacmsframework.model.authority.AdminsRulesModel;
import com.zanha.javacmsframework.model.authority.AdminsRulesSourcesModel;

/**
 * 权限控制相关的逻辑啦
 * @author Administrator
 */
public class PermissionsLogic{
	@Inject
    private H.Session session;
	
	@Inject
	ActionContext context;
	
	@Inject
    private EbeanDao<Integer,AdminsRulesModel> adminsRulesDao;
	
	@Inject
    private EbeanDao<Integer,AdminsModel> adminsDao;
	
	private AdminsRulesSourcesLogic adminsRulesSourcesLogic = new AdminsRulesSourcesLogic();
	
	/**
	 * 获取后台的菜单
	 * @return
	 */
	public List<AdminsRulesSourcesModel> getAdminPermisionsMenus(){
		List<AdminsRulesSourcesModel> permisionsSources = this.getThisAdminThePermisionsSourcesMenus();
		return adminsRulesSourcesLogic.getAllAdminsRulesSources(permisionsSources,0,0);
	}
	
	/**
	 * 获取当前登录用户的所有权限
	 * @return
	 */
	public List<AdminsRulesSourcesModel> getThisAdminThePermisionsSourcesMenus(){		
		AdminsModel loginAdmin = adminsDao.findById(Integer.parseInt(session.get("adminid")));
		AdminsRulesModel rule = adminsRulesDao.findById(loginAdmin.getRuleid());
		String[] sources_results = rule.getSourceids().split(",");
		List<String> sources = Arrays.asList(sources_results);
		return Ebean.find(AdminsRulesSourcesModel.class).where().in("sourceid",sources).eq("isdisplay","1").findList();
	}
	
	/**
	 * 获取当前登录用户的所有权限
	 * @return
	 */
	public List<AdminsRulesSourcesModel> getThisAdminThePermisionsSources(){
		AdminsModel loginAdmin = adminsDao.findById(Integer.parseInt(session.get("adminid")));		
		AdminsRulesModel rule = adminsRulesDao.findById(loginAdmin.getRuleid());
		String[] sources_results = rule.getSourceids().split(",");
		List<String> sources = Arrays.asList(sources_results);
		return Ebean.find(AdminsRulesSourcesModel.class).where().in("sourceid",sources).findList();
	}
	
	/**
	 * 获取所有的权限标示
	 * @return
	 */
	public List<String> getThisAdminThePermisionsSourcesflag(){
		List<String> results = new ArrayList<String>();
		List<AdminsRulesSourcesModel> sources = this.getThisAdminThePermisionsSources();
		for(AdminsRulesSourcesModel source:sources){
			if(null != source.getSourceflag() && !source.getSourceflag().isEmpty()){
				results.add(source.getSourceflag().trim());
				results.add(source.getSourceflag()+"Handler".trim());
			}
		}		
		return results;
	}
	
	/**
	 * 访问权限
	 * @return
	 */
	public boolean checkPermission(){		
		if(this.getThisAdminThePermisionsSourcesflag().contains(context.actionPath().trim())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 访问权限
	 * @return
	 */
	public boolean isPermission(String actionPath){				
		List<String> flags = this.getThisAdminThePermisionsSourcesflag();		
		if(flags.isEmpty() || null == flags){
			return false;
		}
		
		if(flags.contains(actionPath)){
			return true;
		}else{
			return false;
		}
	}
}