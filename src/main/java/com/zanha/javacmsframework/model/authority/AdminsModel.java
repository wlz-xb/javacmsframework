package com.zanha.javacmsframework.model.authority;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.zanha.javacmsframework.util.Power;

@Entity
@Table(name = "zh_admins")
public class AdminsModel {
	@Id
	private Integer adminid;
	private Integer ruleid;
	
	//@Pattern(regexp = "[0-9a-z]{6,24}",message = "用户名的大小必须在6到24之间的字符、由数字或小写字母组成！")
	private String username;
		
	//@Size(min=8,max=30,message = "密码的大小必须在8到30之间的字符！")
	private String password;
	private String safekey;
		
	//@Email(message="请输入正确的邮箱地址")
	private String email;
		
	//@Pattern(regexp = "[0-9]{6,}",message = "请输入正确的qq、qq至少6位以上数字")
	private String qq;
	
	//@Pattern(regexp = "1[0-9]{10}",message = "请输入11位正确的手机号码")
	private String mobile;	
	private Integer islock;
	private Long locktime;
	private Integer isdelete;
	private Long addtime;
	private Long updatetime;

	/**
	 * @return the adminid
	 * @author 粽子
	 */
	public Integer getAdminid() {
		return adminid;
	}

	/**
	 * @param adminid
	 * the adminid to set
	 * @author 粽子
	 */
	public void setAdminid(Integer adminid) {
		this.adminid = adminid;
	}

	/**
	 * @return the ruleid
	 * @author 粽子
	 */
	public Integer getRuleid() {
		return ruleid;
	}

	/**
	 * @param ruleid
	 * the ruleid to set
	 * @author 粽子
	 */
	public void setRuleid(Integer ruleid) {
		this.ruleid = ruleid;
	}

	/**
	 * @return the username
	 * @author 粽子
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 * the username to set
	 * @author 粽子
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 * @author 粽子
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 * the password to set
	 * @author 粽子
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the safekey
	 * @author 粽子
	 */
	public String getSafekey() {
		return safekey;
	}

	/**
	 * @param safekey
	 * the safekey to set
	 * @author 粽子
	 */
	public void setSafekey(String safekey) {
		this.safekey = safekey;
	}

	/**
	 * @return the email
	 * @author 粽子
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 * the email to set
	 * @author 粽子
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the qq
	 * @author 粽子
	 */
	public String getQq() {
		return qq;
	}

	/**
	 * @param qq
	 * the qq to set
	 * @author 粽子
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * @return the mobile
	 * @author 粽子
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 * the mobile to set
	 * @author 粽子
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}	

	/**
	 * @return the islock
	 * @author 粽子
	 */
	public Integer getIslock() {
		return islock;
	}

	/**
	 * @param islock
	 * the islock to set
	 * @author 粽子
	 */
	public void setIslock(Integer islock) {
		this.islock = islock;
	}

	/**
	 * @return the locktime
	 * @author 粽子
	 */
	public Long getLocktime() {
		return locktime;
	}

	/**
	 * @param locktime
	 * the locktime to set
	 * @author 粽子
	 */
	public void setLocktime(Long locktime) {
		this.locktime = locktime;
	}

	/**
	 * @return the isdelete
	 * @author 粽子
	 */
	public Integer getIsdelete() {
		return isdelete;
	}

	/**
	 * @param isdelete
	 * the isdelete to set
	 * @author 粽子
	 */
	public void setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
	}

	/**
	 * @return the addtime
	 * @author 粽子
	 */
	public Long getAddtime() {
		return addtime;
	}

	/**
	 * @param addtime
	 * the addtime to set
	 * @author 粽子
	 */
	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getAddtimeToDate(){
		return Power.stampToDate(addtime);
	}
	
	/**
	 * @return the updatetime
	 * @author 粽子
	 */
	public Long getUpdatetime() {
		return updatetime;
	}
	

	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getUpdatetimeToDate(){
		return Power.stampToDate(updatetime);
	}

	/**
	 * @param updatetime
	 * the updatetime to set
	 * @author 粽子
	 */
	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}	
}