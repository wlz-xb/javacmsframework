package com.zanha.javacmsframework.model.authority;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name = "zh_admins_rules_details")
public class AdminsRulesDetailsModel {
	@Id
	private Integer detailid;
	private Integer ruleid;
	private Integer sourceid;

	/**
	 * @return the detailid
	 * @author 粽子
	 */
	public Integer getDetailid() {
		return detailid;
	}

	/**
	 * @param detailid
	 * the detailid to set
	 * @author 粽子
	 */
	public void setDetailid(Integer detailid) {
		this.detailid = detailid;
	}

	/**
	 * @return the ruleid
	 * @author 粽子
	 */
	public Integer getRuleid() {
		return ruleid;
	}

	/**
	 * @param ruleid
	 * the ruleid to set
	 * @author 粽子
	 */
	public void setRuleid(Integer ruleid) {
		this.ruleid = ruleid;
	}

	/**
	 * @return the sourceid
	 * @author 粽子
	 */
	public Integer getSourceid() {
		return sourceid;
	}

	/**
	 * @param sourceid
	 * the sourceid to set
	 * @author 粽子
	 */
	public void setSourceid(Integer sourceid) {
		this.sourceid = sourceid;
	}
}