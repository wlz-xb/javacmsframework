package com.zanha.javacmsframework.model.authority;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.zanha.javacmsframework.util.Power;
import javax.persistence.Id;

@Entity
@Table(name = "zh_admins_rules")
public class AdminsRulesModel {
	@Id
	private Integer ruleid;
	private String sourceids;
	private String rulename;
	private Long addtime;
	private Long updatetime;

	/**
	 * @return the ruleid
	 * @author 粽子
	 */
	public Integer getRuleid() {
		return ruleid;
	}

	/**
	 * @param ruleid
	 * the ruleid to set
	 * @author 粽子
	 */
	public void setRuleid(Integer ruleid) {
		this.ruleid = ruleid;
	}

	/**
	 * @return the sourceids
	 * @author 粽子
	 */
	public String getSourceids() {
		return sourceids;
	}

	/**
	 * @param sourceids
	 * the sourceids to set
	 * @author 粽子
	 */
	public void setSourceids(String sourceids) {
		this.sourceids = sourceids;
	}

	/**
	 * @return the rulename
	 * @author 粽子
	 */
	public String getRulename() {
		return rulename;
	}

	/**
	 * @param rulename
	 * the rulename to set
	 * @author 粽子
	 */
	public void setRulename(String rulename) {
		this.rulename = rulename;
	}

	/**
	 * @return the addtime
	 * @author 粽子
	 */
	public Long getAddtime() {
		return addtime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getAddtimeToDate(){
		return Power.stampToDate(addtime);
	}

	/**
	 * @param addtime
	 * the addtime to set
	 * @author 粽子
	 */
	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}

	/**
	 * @return the updatetime
	 * @author 粽子
	 */
	public Long getUpdatetime() {
		return updatetime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getUpdatetimeToDate(){
		return Power.stampToDate(updatetime);
	}

	/**
	 * @param updatetime
	 * the updatetime to set
	 * @author 粽子
	 */
	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}
}