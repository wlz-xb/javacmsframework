package com.zanha.javacmsframework.model.authority;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.zanha.javacmsframework.util.Power;
import javax.persistence.Id;

@Entity
@Table(name = "zh_admins_rules_sources")
public class AdminsRulesSourcesModel {
	@Id
	private Integer sourceid;
	private Integer psourceid;
	private Integer level;
	private String sourcetype;
	private String sourcename;
	private String sourceurl;
	private String sourceflag;
	private Integer isdisplay;
	private Integer orderby;
	private Long addtime;
	private Long updatetime;

	/**
	 * @return the sourceid
	 * @author 粽子
	 */
	public Integer getSourceid() {
		return sourceid;
	}

	/**
	 * @param sourceid
	 * the sourceid to set
	 * @author 粽子
	 */
	public void setSourceid(Integer sourceid) {
		this.sourceid = sourceid;
	}

	/**
	 * @return the psourceid
	 * @author 粽子
	 */
	public Integer getPsourceid() {
		return psourceid;
	}

	/**
	 * @param psourceid
	 * the psourceid to set
	 * @author 粽子
	 */
	public void setPsourceid(Integer psourceid) {
		this.psourceid = psourceid;
	}
	
	/**
	 * @return the level
	 * @author 粽子
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level
	 * the level to set
	 * @author 粽子
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
	
	/**
	 * @return the sourcetype
	 * @author 粽子
	 */
	public String getSourcetype() {
		return sourcetype;
	}

	/**
	 * @param sourcetype
	 * the sourcetype to set
	 * @author 粽子
	 */
	public void setSourcetype(String sourcetype) {
		this.sourcetype = sourcetype;
	}

	/**
	 * @return the sourcename
	 * @author 粽子
	 */
	public String getSourcename() {
		return sourcename;
	}

	/**
	 * @param sourcename
	 * the sourcename to set
	 * @author 粽子
	 */
	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}

	/**
	 * @return the sourceurl
	 * @author 粽子
	 */
	public String getSourceurl() {
		return sourceurl;
	}

	/**
	 * @param sourceurl
	 * the sourceurl to set
	 * @author 粽子
	 */
	public void setSourceurl(String sourceurl) {
		this.sourceurl = sourceurl;
	}

	/**
	 * @return the sourceflag
	 * @author 粽子
	 */
	public String getSourceflag() {
		return sourceflag;
	}

	/**
	 * @param sourceflag
	 * the sourceflag to set
	 * @author 粽子
	 */
	public void setSourceflag(String sourceflag) {
		this.sourceflag = sourceflag;
	}

	/**
	 * @return the isdisplay
	 * @author 粽子
	 */
	public Integer getIsdisplay() {
		return isdisplay;
	}

	/**
	 * @param isdisplay
	 * the isdisplay to set
	 * @author 粽子
	 */
	public void setIsdisplay(Integer isdisplay) {
		this.isdisplay = isdisplay;
	}

	/**
	 * @return the orderby
	 * @author 粽子
	 */
	public Integer getOrderby() {
		return orderby;
	}

	/**
	 * @param orderby
	 * the orderby to set
	 * @author 粽子
	 */
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}

	/**
	 * @return the addtime
	 * @author 粽子
	 */
	public Long getAddtime() {
		return addtime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getAddtimeToDate(){
		return Power.stampToDate(addtime);
	}

	/**
	 * @param addtime
	 * the addtime to set
	 * @author 粽子
	 */
	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}

	/**
	 * @return the updatetime
	 * @author 粽子
	 */
	public Long getUpdatetime() {
		return updatetime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getUpdatetimeToDate(){
		return Power.stampToDate(updatetime);
	}

	/**
	 * @param updatetime
	 * the updatetime to set
	 * @author 粽子
	 */
	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}
}