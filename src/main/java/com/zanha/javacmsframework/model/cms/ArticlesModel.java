package com.zanha.javacmsframework.model.cms;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.zanha.javacmsframework.util.Power;
import javax.persistence.Id;

@Entity
@Table(name = "zh_articles")
public class ArticlesModel {
	@Id
	private Integer id;
	private Integer cid;
	private String title;
	private String content;
	private String spiderurl;
	private Long addtime;
	private Long updatetime;	

	/**
	 * @return the id
	 * @author 粽子
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 * the id to set
	 * @author 粽子
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 * @author 粽子
	 */
	public Integer getCid() {
		return cid;
	}

	/**
	 * @param id
	 * the id to set
	 * @author 粽子
	 */
	public void setCid(Integer cid) {
		this.cid = cid;
	}

	/**
	 * @return the title
	 * @author 粽子
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 * the title to set
	 * @author 粽子
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the content
	 * @author 粽子
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 * the content to set
	 * @author 粽子
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the spiderurl
	 * @author 粽子
	 */
	public String getSpiderurl() {
		return spiderurl;
	}

	/**
	 * @param spiderurl
	 * the spiderurl to set
	 * @author 粽子
	 */
	public void setSpiderurl(String spiderurl) {
		this.spiderurl = spiderurl;
	}

	/**
	 * @return the addtime
	 * @author 粽子
	 */
	public Long getAddtime() {
		return addtime;
	}

	/**
	 * @param addtime
	 * the addtime to set
	 * @author 粽子
	 */
	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getAddtimeToDate(){
		return Power.stampToDate(addtime);
	}
	
	/**
	 * @return the updatetime
	 * @author 粽子
	 */
	public Long getUpdatetime() {
		return updatetime;
	}
	

	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getUpdatetimeToDate(){
		return Power.stampToDate(updatetime);
	}

	/**
	 * @param updatetime
	 * the updatetime to set
	 * @author 粽子
	 */
	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}
}