package com.zanha.javacmsframework.model.shop;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.zanha.javacmsframework.util.Power;
import javax.persistence.Id;

@Entity
@Table(name = "zh_goods_categorys")
public class GoodsCategorysModel {
	@Id
	private Integer cid;
	private Integer pid;
	private Integer level;
	private String name;
	private String ename;
	private String path;
	private Long addtime;
	private Long updatetime;

	/**
	 * @return the cid
	 * @author 粽子
	 */
	public Integer getCid() {
		return cid;
	}

	/**
	 * @param cid
	 * the cid to set
	 * @author 粽子
	 */
	public void setCid(Integer cid) {
		this.cid = cid;
	}

	/**
	 * @return the pid
	 * @author 粽子
	 */
	public Integer getPid() {
		return pid;
	}

	/**
	 * @param pid
	 * the pid to set
	 * @author 粽子
	 */
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	
	/**
	 * @return the level
	 * @author 粽子
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level
	 * the level to set
	 * @author 粽子
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * @return the name
	 * @author 粽子
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * the name to set
	 * @author 粽子
	 */
	public void setName(String name) {
		this.name = name;
	}	

	/**
	 * @return the ename
	 * @author 粽子
	 */
	public String getEname() {
		return ename;
	}

	/**
	 * @param ename
	 * the ename to set
	 * @author 粽子
	 */
	public void setEname(String ename) {
		this.ename = ename;
	}
	
	/**
	 * @return the ename
	 * @author 粽子
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * @param path
	 * the path to set
	 * @author 粽子
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * @return the addtime
	 * @author 粽子
	 */
	public Long getAddtime() {
		return addtime;
	}

	/**
	 * @param addtime
	 * the addtime to set
	 * @author 粽子
	 */
	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getAddtimeToDate(){
		return Power.stampToDate(addtime);
	}
	
	/**
	 * @return the updatetime
	 * @author 粽子
	 */
	public Long getUpdatetime() {
		return updatetime;
	}
	

	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getUpdatetimeToDate(){
		return Power.stampToDate(updatetime);
	}

	/**
	 * @param updatetime
	 * the updatetime to set
	 * @author 粽子
	 */
	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}
}