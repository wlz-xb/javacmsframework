package com.zanha.javacmsframework.model.shop;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.zanha.javacmsframework.util.Power;

@Entity
@Table(name = "zh_goods")
public class GoodsModel {
	@Id
	private Integer gid;
	private Integer cid;
	private String title;
	private String content;
	private Long addtime;
	private Long updatetime;

	public Integer getGid() {
		return gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


	/**
	 * @return the addtime
	 * @author 粽子
	 */
	public Long getAddtime() {
		return addtime;
	}

	/**
	 * @param addtime
	 * the addtime to set
	 * @author 粽子
	 */
	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}
	
	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getAddtimeToDate(){
		return Power.stampToDate(addtime);
	}
	
	/**
	 * @return the updatetime
	 * @author 粽子
	 */
	public Long getUpdatetime() {
		return updatetime;
	}
	

	/**
	 * 获取时间格式
	 * @author 粽子
	 */
	public String getUpdatetimeToDate(){
		return Power.stampToDate(updatetime);
	}

	/**
	 * @param updatetime
	 * the updatetime to set
	 * @author 粽子
	 */
	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}
}