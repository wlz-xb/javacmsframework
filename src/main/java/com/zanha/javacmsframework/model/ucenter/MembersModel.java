package com.zanha.javacmsframework.model.ucenter;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
@Entity
@Table(name = "zh_members")
public class MembersModel {
	@Id
	private Integer memberid;
	private String username;
	private String password;
	private Long addtime;
	private Long updatetime;

	/**
	 * @return the memberid
	 * @author 粽子
	 */
	public Integer getMemberid() {
		return memberid;
	}

	/**
	 * @param memberid
	 * the memberid to set
	 * @author 粽子
	 */
	public void setMemberid(Integer memberid) {
		this.memberid = memberid;
	}

	/**
	 * @return the username
	 * @author 粽子
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 * the username to set
	 * @author 粽子
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 * @author 粽子
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 * the password to set
	 * @author 粽子
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the addtime
	 * @author 粽子
	 */
	public Long getAddtime() {
		return addtime;
	}

	/**
	 * @param addtime the addtime to set
	 * @author 粽子
	 */
	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}

	/**
	 * @return the updatetime
	 * @author 粽子
	 */
	public Long getUpdatetime() {
		return updatetime;
	}

	/**
	 * @param updatetime the updatetime to set
	 * @author 粽子
	 */
	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}
}