package com.zanha.javacmsframework.util;
import java.beans.PropertyDescriptor;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * null的情况
 * @author 粽子
 */
public class BBeanUtils extends org.apache.commons.beanutils.BeanUtils {
	/**
	 * null排除
	 * @param databean 这个是form提交过来的数据新数据
	 * @param tobean 这个是历史数据
	 * @throws Exception
	 */
	public static void copyPropertyss(Object databean, Object tobean) throws Exception {
		PropertyDescriptor origDescriptors[] = PropertyUtils.getPropertyDescriptors(databean);
		for (int i = 0; i < origDescriptors.length; i++) {
			String name = origDescriptors[i].getName();
			// String type = origDescriptors[i].getPropertyType().toString();
			if ("class".equals(name)) {
				continue; // No point in trying to set an object's class
			}
			if (PropertyUtils.isReadable(databean, name) && PropertyUtils.isWriteable(tobean, name)) {
				Object value = PropertyUtils.getSimpleProperty(databean, name);
				if (value != null) {
					copyProperty(tobean, name, value);
				}
			}
		}
	}
}