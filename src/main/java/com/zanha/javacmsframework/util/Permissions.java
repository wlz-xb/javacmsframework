package com.zanha.javacmsframework.util;
import javax.inject.Inject;
import com.zanha.javacmsframework.logic.PermissionsLogic;

/**
 * 页面上权限控制啦
 * @author Administrator
 */
public class Permissions {	
	@Inject
	PermissionsLogic permissionsLogic;
	
	/**
	 * 如果有权限就在页面上显示按钮，文字，链接啦
	 * @param actionPath
	 * @return
	 */
	public boolean isPermission(String actionPath){	
		//System.out.println("dddddddddddd");
		//PermissionsLogic permissionsLogic = new PermissionsLogic();
		return permissionsLogic.isPermission(actionPath);
	}
}