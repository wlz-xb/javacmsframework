package com.zanha.javacmsframework.util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Power{
	/**
	 * 简化输出system.out.println方法，类似php的 echo方法
	 * @param result
	 */
	public static void sop(Object result){
		System.out.println(result);
	}
	
	/* 
     * 将时间转换为时间戳
     * @author 粽子
     */    
    public static Long dateToStamp(String s) throws ParseException{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        Date date = simpleDateFormat.parse(s);
        long ts = date.getTime();
        return ts;
    }
    
    /* 
     * 将时间戳转换为时间
     * @author 粽子
     */
    public static String stampToDate(Long lt){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }
	
	/**
	 * 获取当前的时间
	 * @return long
	 */
	public static long getThisTime(){
		Date d = new Date();
		return d.getTime();
	}
	
	/**
	 * 重复只付出n次啦
	 * @param str
	 * @param n
	 * @return
	 */
	public static String addStrings(String str , int n){
		String tmpStr = "";
		for(int i=0;i<n;i++){
			tmpStr += str;
		}
		return tmpStr;
	}
	
	/**
     * 字符串替换
     * @param src
     * @param target
     * @param replacement
     * @return
     */
    public static String replaceAllStr(String src, String target, String replacement) {
        if (src == null || target == null || replacement == null) return src;
        int idx = src.indexOf(target);
        if (idx == -1) return src;
        int pst = 0;
        char[] cs = src.toCharArray();
        char[] rs = new char[src.length() - target.length() + replacement.length()];
        for (int i = 0; i < cs.length; i ++) {
            if (i == idx) {
                for (char c : replacement.toCharArray()) {
                    rs[pst] = c;
                    pst ++;
                }
                continue;
            }
            if (i > idx && i < idx + target.length()) continue;
            rs[pst] = cs[i];
            pst ++;
        }
        return replaceAllStr(new String(rs), target, replacement);
    }

    /**
     * 随机字符啦
     * @param length
     * @return
     */
    public static String getRandomString(int length) {
        StringBuffer buffer = new StringBuffer("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        int range = buffer.length();
        for (int i = 0; i < length; i ++) {
            sb.append(buffer.charAt(random.nextInt(range)));
        }
        return sb.toString();
    }
}