/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50617
Source Host           : 192.168.0.145:3306
Source Database       : zanha

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2017-03-21 15:00:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `zh_admins`
-- ----------------------------
DROP TABLE IF EXISTS `zh_admins`;
CREATE TABLE `zh_admins` (
  `adminid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ruleid` int(10) unsigned DEFAULT '0' COMMENT '所属角色',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(34) DEFAULT NULL COMMENT '密码',
  `safekey` varchar(10) DEFAULT NULL COMMENT '密码安全码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `qq` bigint(20) unsigned DEFAULT '0' COMMENT 'QQ',
  `mobile` bigint(20) unsigned DEFAULT '0' COMMENT '手机',
  `islock` tinyint(2) unsigned DEFAULT '0' COMMENT '锁定',
  `locktime` bigint(20) unsigned DEFAULT '0' COMMENT '锁定时间，30分钟后自动解开',
  `isdelete` tinyint(2) unsigned DEFAULT '0' COMMENT '删除掉',
  `addtime` bigint(20) unsigned DEFAULT '0' COMMENT '添加时间',
  `updatetime` bigint(20) unsigned DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`adminid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zh_admins
-- ----------------------------
INSERT INTO `zh_admins` VALUES ('1', '1', 'testtest', '4uC30tNkP53UBEeIJ1i6vg==', 'ToT9F6', '1598135958@qq.com', '1598135958', '18657616521', '0', '0', '0', '1489475744053', '1490079418191');

-- ----------------------------
-- Table structure for `zh_admins_rules`
-- ----------------------------
DROP TABLE IF EXISTS `zh_admins_rules`;
CREATE TABLE `zh_admins_rules` (
  `ruleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sourceids` text COMMENT '权限资源id集合',
  `rulename` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `addtime` bigint(20) unsigned DEFAULT '0' COMMENT '添加时间',
  `updatetime` bigint(20) unsigned DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`ruleid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zh_admins_rules
-- ----------------------------
INSERT INTO `zh_admins_rules` VALUES ('1', '1,2,3,8,9,10,4,6,7,11,12,5,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29', '普通会员', '0', '1489980396077');

-- ----------------------------
-- Table structure for `zh_admins_rules_details`
-- ----------------------------
DROP TABLE IF EXISTS `zh_admins_rules_details`;
CREATE TABLE `zh_admins_rules_details` (
  `detailid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ruleid` int(10) unsigned DEFAULT NULL COMMENT '角色id',
  `sourceid` int(10) unsigned DEFAULT NULL COMMENT '权限资源id',
  PRIMARY KEY (`detailid`)
) ENGINE=InnoDB AUTO_INCREMENT=323 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zh_admins_rules_details
-- ----------------------------
INSERT INTO `zh_admins_rules_details` VALUES ('294', '1', '1');
INSERT INTO `zh_admins_rules_details` VALUES ('295', '1', '2');
INSERT INTO `zh_admins_rules_details` VALUES ('296', '1', '3');
INSERT INTO `zh_admins_rules_details` VALUES ('297', '1', '8');
INSERT INTO `zh_admins_rules_details` VALUES ('298', '1', '9');
INSERT INTO `zh_admins_rules_details` VALUES ('299', '1', '10');
INSERT INTO `zh_admins_rules_details` VALUES ('300', '1', '4');
INSERT INTO `zh_admins_rules_details` VALUES ('301', '1', '6');
INSERT INTO `zh_admins_rules_details` VALUES ('302', '1', '7');
INSERT INTO `zh_admins_rules_details` VALUES ('303', '1', '11');
INSERT INTO `zh_admins_rules_details` VALUES ('304', '1', '12');
INSERT INTO `zh_admins_rules_details` VALUES ('305', '1', '5');
INSERT INTO `zh_admins_rules_details` VALUES ('306', '1', '13');
INSERT INTO `zh_admins_rules_details` VALUES ('307', '1', '14');
INSERT INTO `zh_admins_rules_details` VALUES ('308', '1', '15');
INSERT INTO `zh_admins_rules_details` VALUES ('309', '1', '16');
INSERT INTO `zh_admins_rules_details` VALUES ('310', '1', '17');
INSERT INTO `zh_admins_rules_details` VALUES ('311', '1', '18');
INSERT INTO `zh_admins_rules_details` VALUES ('312', '1', '19');
INSERT INTO `zh_admins_rules_details` VALUES ('313', '1', '20');
INSERT INTO `zh_admins_rules_details` VALUES ('314', '1', '21');
INSERT INTO `zh_admins_rules_details` VALUES ('315', '1', '22');
INSERT INTO `zh_admins_rules_details` VALUES ('316', '1', '23');
INSERT INTO `zh_admins_rules_details` VALUES ('317', '1', '24');
INSERT INTO `zh_admins_rules_details` VALUES ('318', '1', '25');
INSERT INTO `zh_admins_rules_details` VALUES ('319', '1', '26');
INSERT INTO `zh_admins_rules_details` VALUES ('320', '1', '27');
INSERT INTO `zh_admins_rules_details` VALUES ('321', '1', '28');
INSERT INTO `zh_admins_rules_details` VALUES ('322', '1', '29');

-- ----------------------------
-- Table structure for `zh_admins_rules_sources`
-- ----------------------------
DROP TABLE IF EXISTS `zh_admins_rules_sources`;
CREATE TABLE `zh_admins_rules_sources` (
  `sourceid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `psourceid` int(10) unsigned DEFAULT '0' COMMENT '所属资源',
  `level` int(10) unsigned DEFAULT '0' COMMENT '级别',
  `sourcetype` tinyint(3) unsigned DEFAULT NULL COMMENT '资源类型1系统，2菜单，3按钮，4数据',
  `sourcename` varchar(255) DEFAULT NULL COMMENT '资源名称',
  `sourceurl` varchar(255) DEFAULT NULL COMMENT '资源网址',
  `sourceflag` varchar(255) DEFAULT NULL COMMENT '资源标识',
  `isdisplay` tinyint(2) unsigned DEFAULT '1' COMMENT '是否显示，1显示，0隐藏',
  `orderby` int(10) unsigned DEFAULT '0' COMMENT '排序',
  `addtime` bigint(20) unsigned DEFAULT '0' COMMENT '添加时间',
  `updatetime` bigint(20) unsigned DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`sourceid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zh_admins_rules_sources
-- ----------------------------
INSERT INTO `zh_admins_rules_sources` VALUES ('1', '0', '0', '1', '权限管理', '/admin/authority/AdminsRulesSources/index', 'authority.AdminsRulesSources.index', '1', '1', '0', '1489654610238');
INSERT INTO `zh_admins_rules_sources` VALUES ('2', '1', '0', '1', '权限资源', '/admin/authority/AdminsRulesSources/index', 'authority.AdminsRulesSources.index', '1', '1', '1', '1489654242161');
INSERT INTO `zh_admins_rules_sources` VALUES ('3', '2', '0', '1', '权限资源列表', '/admin/authority/AdminsRulesSources/index', 'authority.AdminsRulesSources.index', '1', '1', '0', '1489654282961');
INSERT INTO `zh_admins_rules_sources` VALUES ('4', '1', '0', '1', '权限角色', '/admin/authority/AdminsRules/index', 'authority.AdminsRules.index', '1', '1', '0', '1489654803158');
INSERT INTO `zh_admins_rules_sources` VALUES ('5', '1', '0', '1', '权限用户', '/admin/authority/Admins/index', 'authority.Admins.index', '1', '0', '0', '1489660950593');
INSERT INTO `zh_admins_rules_sources` VALUES ('6', '4', '0', '1', '权限角色列表', '/admin/authority/AdminsRules/index', 'authority.AdminsRules.index', '1', '0', '0', '1489653536044');
INSERT INTO `zh_admins_rules_sources` VALUES ('7', '6', '0', '1', '添加权限角色', '/admin/authority/AdminsRules/add', 'authority.AdminsRules.add', '1', '0', '0', '1489653553166');
INSERT INTO `zh_admins_rules_sources` VALUES ('8', '3', null, '1', '添加权限资源', '/admin/authority/AdminsRulesSources/add', 'authority.AdminsRulesSources.add', '1', '1', '1489653406935', '1489653406935');
INSERT INTO `zh_admins_rules_sources` VALUES ('9', '3', null, '1', '修改权限资源', '/admin/authority/AdminsRulesSources/edit', 'authority.AdminsRulesSources.edit', '1', '1', '1489653422433', '1489653422433');
INSERT INTO `zh_admins_rules_sources` VALUES ('10', '3', null, '1', '删除权限资源', '/admin/authority/AdminsRulesSources/delete', 'authority.AdminsRulesSources.delete', '1', '1', '1489653447313', '1489653447313');
INSERT INTO `zh_admins_rules_sources` VALUES ('11', '6', null, '1', '修改权限角色', '/admin/authority/AdminsRules/edit', 'authority.AdminsRules.edit', '1', '1', '1489653573467', '1489653573467');
INSERT INTO `zh_admins_rules_sources` VALUES ('12', '6', null, '1', '删除权限角色', '/admin/authority/AdminsRules/delete', 'authority.AdminsRules.delete', '1', '1', '1489653593699', '1489653593699');
INSERT INTO `zh_admins_rules_sources` VALUES ('13', '5', null, '1', '权限用户列表', '/admin/authority/Admins/index', 'authority.Admins.index', '1', '1', '1489653684454', '1489654865768');
INSERT INTO `zh_admins_rules_sources` VALUES ('14', '13', null, '1', '添加权限用户', '/admin/authority/Admins/add', 'authority.Admins.add', '1', '1', '1489653724856', '1489653724856');
INSERT INTO `zh_admins_rules_sources` VALUES ('15', '13', null, '1', '修改权限用户', '/admin/authority/Admins/edit', 'authority.Admins.edit', '1', '1', '1489653753184', '1489653753184');
INSERT INTO `zh_admins_rules_sources` VALUES ('16', '13', null, '1', '删除权限用户', '/admin/authority/Admins/delete', 'authority.Admins.delete', '1', '1', '1489653773540', '1489653773540');
INSERT INTO `zh_admins_rules_sources` VALUES ('17', '0', null, '1', '内容管理', '/admin/cms/Articles/index', 'cms.Articles.index', '1', '1', '1489653812910', '1489653858059');
INSERT INTO `zh_admins_rules_sources` VALUES ('18', '17', null, '1', '文章管理', '/admin/cms/Articles/index', 'cms.Articles.index', '1', '1', '1489653849744', '1489653849744');
INSERT INTO `zh_admins_rules_sources` VALUES ('19', '18', null, '1', '文章列表', '/admin/cms/Articles/index', 'cms.Articles.index', '1', '1', '1489653894890', '1489653901840');
INSERT INTO `zh_admins_rules_sources` VALUES ('20', '19', null, '1', '添加文章', '/admin/cms/Articles/add', 'cms.Articles.add', '1', '1', '1489653948367', '1489653948367');
INSERT INTO `zh_admins_rules_sources` VALUES ('21', '19', null, '1', '修改文章', '/admin/cms/Articles/edit', 'cms.Articles.edit', '1', '1', '1489653971021', '1489653971021');
INSERT INTO `zh_admins_rules_sources` VALUES ('22', '19', null, '1', '删除文章', '/admin/cms/Articles/delete', 'cms.Articles.delete', '1', '1', '1489653992546', '1489653992546');
INSERT INTO `zh_admins_rules_sources` VALUES ('23', '17', null, '1', '文章分类', '/admin/cms/Categorys/index', 'cms.Categorys.index', '1', '1', '1489654033588', '1489654033588');
INSERT INTO `zh_admins_rules_sources` VALUES ('24', '23', null, '1', '分类列表', '/admin/cms/Categorys/index', 'cms.Categorys.index', '1', '1', '1489654063123', '1489654063123');
INSERT INTO `zh_admins_rules_sources` VALUES ('25', '24', null, '1', '添加分类', '/admin/cms/Categorys/add', 'cms.Categorys.add', '1', '1', '1489654087437', '1489654116711');
INSERT INTO `zh_admins_rules_sources` VALUES ('26', '24', null, '1', '修改分类', '/admin/cms/Categorys/edit', 'cms.Categorys.edit', '1', '1', '1489654147357', '1489654147357');
INSERT INTO `zh_admins_rules_sources` VALUES ('27', '24', null, '1', '删除分类', '/admin/cms/Categorys/delete', 'cms.Categorys.delete', '1', '1', '1489654199511', '1489654199511');
INSERT INTO `zh_admins_rules_sources` VALUES ('28', '0', null, '1', '我的桌面', '/admin/Index/index', 'Index.index', '0', '1', '1489657837770', '1489657837770');
INSERT INTO `zh_admins_rules_sources` VALUES ('29', '0', null, '1', '欢迎首页', '/admin/Index/welcome', 'Index.welcome', '0', '1', '1489657881600', '1489657881600');
